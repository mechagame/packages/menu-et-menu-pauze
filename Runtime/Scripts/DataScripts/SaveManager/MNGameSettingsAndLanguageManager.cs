﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//|                                                                             This Library is made For Mecha Nichon                                                                       | 
//|                                                                                                                                                                                       |
//|                                                                                 Copyright Mecha Boullette 2020                                                                           | 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MechaBoullette.MechaNichon.Game
{
    public class MNGameSettingsAndLanguageManager : MonoBehaviour
    {
        public static MNGameSettingsAndLanguageManager MNGameSettingsAndLanguageManagerInstance = new MNGameSettingsAndLanguageManager();

        #region SERIALIZED FIELDS
        [SerializeField] private string JsonSaveFile;

        #endregion

        #region PRIVATE FIELDS
        private int currentLanguage;
        #endregion

        #region PUBLIC PROPERTIES
        public int SetCurrentLanguage
        {
            set
            {
                currentLanguage = value;
            }
        }

        public int GetCurrentLanguage
        {
            get
            {
                return currentLanguage;
            }
        }
        #endregion

        #region PUBLIC FUNCTIONS

        #endregion

        #region EVENTS
        #endregion

        #region PRIVATE FUNCTIONS

          // Start is called before the first frame update
    void Start()
    {
        if(MNGameSettingsAndLanguageManagerInstance != null)
            {
                Destroy(this);
            }
            else
            {
                MNGameSettingsAndLanguageManagerInstance = this;
            }

        // Set the Saved Language as default
            SetCurrentLanguage = LoadSavedLanguage();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

        private int LoadSavedLanguage()
        {
            // load saved language value from settings json
            var _languageNumber = 0;
            return _languageNumber;
        }
        #endregion


    }
}
