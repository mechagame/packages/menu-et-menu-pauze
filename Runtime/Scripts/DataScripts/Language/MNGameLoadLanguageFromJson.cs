﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//|                                                                             This Library is made For Mecha Nichon                                                                       | 
//|                                                                                                                                                                                       |
//|                                                                                 Copyright Mecha Boullette 2020                                                                           | 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace MechaBoullette.MechaNichon.Game
{
    public class MNGameLoadLanguageFromJson : MonoBehaviour
    {
        #region SERIALIZED FIELDS
        #endregion

        #region PRIVATE FIELDS
        private MNCurrentLanguage currentLanguage;
        private string path;
        #endregion

        #region PUBLIC PROPERTIES
        #endregion

        #region PUBLIC FUNCTIONS
        #endregion

        #region EVENTS
        #endregion

        #region PRIVATE FUNCTIONS

          // Start is called before the first frame update
    void Start()
    {
            currentLanguage = new MNCurrentLanguage();
            path = Application.streamingAssetsPath + "/Language.json";
            ReadCurrentLanguageJson();
    }

    private void ReadCurrentLanguageJson()
        {
            var _jsonstring = File.ReadAllText(path);
            currentLanguage = JsonUtility.FromJson<MNCurrentLanguage>(_jsonstring);
            MNGameLanguageManager.MNGameLanguageManagerInstance.SetLanguageCurrent = currentLanguage.CurrentLanguage;
        }

      public void WriteCurrentLanguageJson()
        {
            MNCurrentLanguage _tempWrite = new MNCurrentLanguage();
            _tempWrite.CurrentLanguage = MNGameLanguageManager.MNGameLanguageManagerInstance.GetLanguageCurrent;
            string json = JsonUtility.ToJson(_tempWrite);

            File.WriteAllText(path, json);
        }

        private void OnApplicationQuit()
        {
            WriteCurrentLanguageJson();
        }
        // Update is called once per frame
        void Update()
    {
        
    }
        #endregion


    }
}
