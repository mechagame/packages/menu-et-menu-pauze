﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//|                                                                             This Library is made For Mecha Nichon                                                                       | 
//|                                                                                                                                                                                       |
//|                                                                                 Copyright Mecha Boullette 2020                                                                           | 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MechaBoullette.MechaNichon.Game
{
    public class MNGameSelectLanguage : MonoBehaviour
    {
        #region SERIALIZED FIELDS
        #endregion

        #region PRIVATE FIELDS
        #endregion

        #region PUBLIC PROPERTIES
        #endregion

        #region PUBLIC FUNCTIONS
        public void SetLanguage(int value)
        {
            MNGameLanguageManager.MNGameLanguageManagerInstance.SetLanguageCurrent = value;
            Debug.Log(value);
        }


        #endregion

        #region EVENTS
        #endregion

        #region PRIVATE FUNCTIONS

          // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
        #endregion


    }
}
