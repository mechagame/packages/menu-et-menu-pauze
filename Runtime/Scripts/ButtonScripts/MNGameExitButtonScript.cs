﻿//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//|                                                                             This Library is made For Mecha Nichon                                                                     | 
//|                                                                                                                                                                                       |
//|                                                                                 Copyright Mecha Boullette 2020                                                                        | 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MechaBoullette.MechaNichon.Game
{
    public class MNGameExitButtonScript : MonoBehaviour
    {
        #region SERIALIZED FIELDS
        #endregion

        #region PRIVATE FIELDS
        #endregion

        #region PUBLIC PROPERTIES
        #endregion

        #region PUBLIC FUNCTIONS

        public void ExitGame()
        {
            Application.Quit();
        }

        #endregion

        #region EVENTS
        #endregion

        #region PRIVATE FUNCTIONS

     
        #endregion


    }
}
